| Algorithm  | Length = 10^2 | Length = 10^3 | Length = 10^4 | Length = 10^5 | Length = 10^6 |
|------------|---------------|---------------|---------------|---------------|---------------|
| Quick Sort | 0 ms.         | 0 ms.         | 2 ms.         | 28 ms.        | 535 ms.       |  
| Merge Sort | 0 ms.         | 0 ms.         | 2 ms.         | 23 ms.        | 236 ms.       |