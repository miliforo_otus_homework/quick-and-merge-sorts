#include <cstdlib>
#include <iostream>
#include <ostream>
#include <chrono>

using namespace std;
#define time chrono::time_point<chrono::system_clock>

time Now() { return chrono::system_clock::now(); }

void Merge(int* Array, int Left, int X, int Right)
{
    auto TempArray = new int[Right - Left + 1];
    int LeftSide = Left, RightSide = X + 1, PivotIndex = 0;

    while (LeftSide <= X && RightSide <= Right)
    {
        Array[LeftSide] < Array[RightSide] ? TempArray[PivotIndex++] = Array[LeftSide++] : TempArray[PivotIndex++] = Array[RightSide++];
    }

    while (LeftSide <= X)
    {
        TempArray[PivotIndex++] = Array[LeftSide++];
    }

    while (RightSide <= Right)
    {
        TempArray[PivotIndex++] = Array[RightSide++];
    }

    for (int i = Left; i <= Right; i++)
    {
        Array[i] = TempArray[i - Left];
    }
}

void MergeSortHelper(int* Array, int Left, int Right)
{
    if (Left >= Right)
    {
        return;
    }

    int Middle = (Left + Right) / 2;
    MergeSortHelper(Array, Left, Middle);
    MergeSortHelper(Array, Middle + 1, Right);
    Merge(Array, Left, Middle, Right);
}

void MergeSort(int* Array, int Length)
{
    MergeSortHelper(Array, 0, Length - 1);
}

int Split(int* Array, int Left, int Right)
{
    int Pivot = Array[Right];
    int Middle = Left - 1;

    for (int i = Left; i <= Right; i++)
    {
        if (Array[i] <= Pivot)
        {
            swap(Array[++Middle], Array[i]);
        }
    }

    return Middle;
}

void QuickSortHelper(int* Array, int Left, int Right)
{
    if (Left >= Right)
    {
        return;
    }

    int Middle = Split(Array, Left, Right);
    QuickSortHelper(Array, Left, Middle - 1);
    QuickSortHelper(Array, Middle + 1, Right);
}

void QuickSort(int* Array, int Length)
{
    QuickSortHelper(Array, 0, Length - 1);
}


void PrintLine()
{
    cout << "______" << endl;
}

void PrintArray(int* Array, int Length)
{
    for (int i = 0; i < Length; i++)
    {
        cout << Array[i] << endl;
    }
    PrintLine();
}

void InitializeRandomArray(int* Array, int Length)
{
    for (int i = 0; i < Length; i++)
    {
        Array[i] = rand();
    }
}

int main(int argc, char* argv[])
{
    chrono::time_point<chrono::system_clock> StartTime;
    
    const int Length = pow(10, 6);
    int* Array = new int[Length];
    InitializeRandomArray(Array, Length);
    StartTime = Now();
    // PrintArray(Array, Length);
    // QuickSort(Array, Length);
    // MergeSort(Array, Length);
    cout << chrono::duration_cast<std::chrono::milliseconds>(Now() - StartTime).count() << " ms" << endl;
    
    // PrintArray(Array, Length);
    
    return 0;
}
